n = 25;  # number of years
H = 100.;
borehole_extra_info = [1.; 0.; 0.];

field_characteristics = [1. 0. 10. 2. 150. 150*8760/1000;
                        1. 20. 0. 4. 180. 180*8760/1000;
                        1. 45. 0.0 6.0 300. 300*8760/1000;
                        1. 2. 25. 2.0 170. 170*8760/1000;
                        1. 5. 50. 1.0 200. 200*8760/1000;
                        1. 20. 40. 5.0 250. 250*8760/1000];
# goal_borehole_characteristics = [borehole_extra_info[1] borehole_extra_info[2] borehole_extra_info[3] D H H*8.760];

resIV = groundheatpump_IV(n, field_characteristics, H, borehole_extra_info)


# field_characteristics = [1. 0. 10. 2. 152. 150*8760/1000;
#                         1. 20. 0. 4. 184. 180*8760/1000;
#                         1. 45. 0.0 6.0 306. 300*8760/1000;
#                         1. 2. 25. 2.0 172. 170*8760/1000;
#                         1. 5. 50. 1.0 201. 200*8760/1000;
#                         1. 20. 40. 5.0 255. 250*8760/1000];

resI = groundheatpump_I(n, field_characteristics, H, borehole_extra_info)
El_calculatedI = sum(resI[1] .+ resI[2] .+ resI[4] .+ resI[5])
El_calculatedIV = sum(resIV[1] .+ resIV[2] .+ resIV[4] .+ resIV[5])

resI[2] .- resIV[3]

plot(resI[2])
plot!(resIV[3])

plot(resI[1])
plot!(resIV[1])

maximum(abs.(resI[1] .- resIV[1]))
