#test1 square
    R=2.;
    L=4.;

    gsq=square(R,L);

    gsqtrue=fill(Point2{Float64}(0.,0.),9);
    gsqtrue[2]= Point2{Float64}(2.,0.);
    gsqtrue[3]= Point2{Float64}(4.,0.);
    gsqtrue[4]= Point2{Float64}(0.,2.);
    gsqtrue[5]= Point2{Float64}(2.,2.);
    gsqtrue[6]= Point2{Float64}(4.,2.);
    gsqtrue[7]= Point2{Float64}(0.,4.);
    gsqtrue[8]= Point2{Float64}(2.,4.);
    gsqtrue[9]= Point2{Float64}(4.,4.);

    @test gsq  == gsqtrue
#end test square
#test2 square
    R=2.;
    L=5.5;

    gsq=square(R,L);

    gsqtrue=fill(Point2{Float64}(0.,0.),9);
    gsqtrue[2]= Point2{Float64}(2.,0.);
    gsqtrue[3]= Point2{Float64}(4.,0.);
    gsqtrue[4]= Point2{Float64}(0.,2.);
    gsqtrue[5]= Point2{Float64}(2.,2.);
    gsqtrue[6]= Point2{Float64}(4.,2.);
    gsqtrue[7]= Point2{Float64}(0.,4.);
    gsqtrue[8]= Point2{Float64}(2.,4.);
    gsqtrue[9]= Point2{Float64}(4.,4.);

    @test gsq  == gsqtrue
#end test2 square

#test square
    R=2.;
    N=3;

    gsq=square(R,N);

    gsqtrue=fill(Point2{Float64}(0.,0.),9);
    gsqtrue[2]= Point2{Float64}(2.,0.);
    gsqtrue[3]= Point2{Float64}(4.,0.);
    gsqtrue[4]= Point2{Float64}(0.,2.);
    gsqtrue[5]= Point2{Float64}(2.,2.);
    gsqtrue[6]= Point2{Float64}(4.,2.);
    gsqtrue[7]= Point2{Float64}(0.,4.);
    gsqtrue[8]= Point2{Float64}(2.,4.);
    gsqtrue[9]= Point2{Float64}(4.,4.);

    @test gsq  == gsqtrue
#end test square

#test1 circle
    R=2.;
    L=3.;

    gsq=circle(R,L);

    gsqtrue=fill(Point2{Float64}(0.,0.),1,9);
    gsqtrue[1]= Point2{Float64}(0.,0.);
    gsqtrue[2]= Point2{Float64}(0.,2.);
    gsqtrue[3]= Point2{Float64}(0.,4.);
    gsqtrue[4]= Point2{Float64}(2.,0.);
    gsqtrue[5]= Point2{Float64}(2.,2.);
    gsqtrue[6]= Point2{Float64}(2.,4.);
    gsqtrue[7]= Point2{Float64}(4.,0.);
    gsqtrue[8]= Point2{Float64}(4.,2.);
    gsqtrue[9]= Point2{Float64}(4.,4.);

#end test circle

#test2 circle
    R=2.;
    L=6.;

    gsq=circle(R,L);

    gsqtrue=fill(Point2{Float64}(0.,0.),1,13);
    gsqtrue[1]= Point2{Float64}(4.,0.);
    gsqtrue[2]= Point2{Float64}(2.,2.);
    gsqtrue[3]= Point2{Float64}(4.,2.);
    gsqtrue[4]= Point2{Float64}(6.,2.);
    gsqtrue[5]= Point2{Float64}(0.,4.);
    gsqtrue[6]= Point2{Float64}(2.,4.);
    gsqtrue[7]= Point2{Float64}(4.,4.);
    gsqtrue[8]= Point2{Float64}(6.,4.);
    gsqtrue[9]= Point2{Float64}(8.,4.);
    gsqtrue[10]= Point2{Float64}(2.,6.);
    gsqtrue[11]= Point2{Float64}(4.,6.);
    gsqtrue[12]= Point2{Float64}(6.,6.);
    gsqtrue[13]= Point2{Float64}(4.,8.);
#end test circle

#test prism
    R=2.;
    L=5.;
    D=0.;
    gsq=prism(R,L,D);

    gsqtrue=fill(Point3{Float64}(0.,0.,0.),9);
    gsqtrue[2]= Point3{Float64}(2.,0.,0.);
    gsqtrue[3]= Point3{Float64}(4.,0.,0.);
    gsqtrue[4]= Point3{Float64}(0.,2.,0.);
    gsqtrue[5]= Point3{Float64}(2.,2.,0.);
    gsqtrue[6]= Point3{Float64}(4.,2.,0.);
    gsqtrue[7]= Point3{Float64}(0.,4.,0.);
    gsqtrue[8]= Point3{Float64}(2.,4.,0.);
    gsqtrue[9]= Point3{Float64}(4.,4.,0.);


    @test gsq  == gsqtrue
#end test prism

#test prism
    R=2.;
    L=3.;
    D=4.;
    H=50.;
    m=2;
    gsq=prism(R,L,D,H,m);

    gsqtrue=fill(Point3{Float64}(0.,0.,0.),8);
    gsqtrue[1]= Point3{Float64}(0.,0.,4.);
    gsqtrue[2]= Point3{Float64}(0.,0.,29.);
    gsqtrue[3]= Point3{Float64}(2.,0.,4.);
    gsqtrue[4]= Point3{Float64}(2.,0.,29.);
    gsqtrue[5]= Point3{Float64}(0.,2.,4.);
    gsqtrue[6]= Point3{Float64}(0.,2.,29.);
    gsqtrue[7]= Point3{Float64}(2.,2.,4.);
    gsqtrue[8]= Point3{Float64}(2.,2.,29.);

    @test gsq  == gsqtrue
#end test prism

#test prism
    R=2.;
    N=3;
    D=0.;

    gsq=prism(R,N,D);

    gsqtrue=fill(Point3{Float64}(0.,0.,0.),9);
    gsqtrue[2]= Point3{Float64}(2.,0.,0.);
    gsqtrue[3]= Point3{Float64}(4.,0.,0.);
    gsqtrue[4]= Point3{Float64}(0.,2.,0.);
    gsqtrue[5]= Point3{Float64}(2.,2.,0.);
    gsqtrue[6]= Point3{Float64}(4.,2.,0.);
    gsqtrue[7]= Point3{Float64}(0.,4.,0.);
    gsqtrue[8]= Point3{Float64}(2.,4.,0.);
    gsqtrue[9]= Point3{Float64}(4.,4.,0.);

    @test gsq  == gsqtrue
#end test prism

#test1 cylinder
R=2.;
L=3.;
D=0.;
gsq=cylinder(R,L,D);

gsqtrue=fill(Point3{Float64}(0.,0.,0.),1,9);
gsqtrue[1]= Point3{Float64}(0.,0.,0.);
gsqtrue[2]= Point3{Float64}(0.,2.,0.);
gsqtrue[3]= Point3{Float64}(0.,4.,0.);
gsqtrue[4]= Point3{Float64}(2.,0.,0.);
gsqtrue[5]= Point3{Float64}(2.,2.,0.);
gsqtrue[6]= Point3{Float64}(2.,4.,0.);
gsqtrue[7]= Point3{Float64}(4.,0.,0.);
gsqtrue[8]= Point3{Float64}(4.,2.,0.);
gsqtrue[9]= Point3{Float64}(4.,4.,0.);

#end test cylinder

#test2 cylinder
R=2.;
L=6.;
D=0.;

gsq=cylinder(R,L,D);

gsqtrue=fill(Point3{Float64}(0.,0.,0.),1,13);
gsqtrue[1]= Point3{Float64}(4.,0.,0.);
gsqtrue[2]= Point3{Float64}(2.,2.,0.);
gsqtrue[3]= Point3{Float64}(4.,2.,0.);
gsqtrue[4]= Point3{Float64}(6.,2.,0.);
gsqtrue[5]= Point3{Float64}(0.,4.,0.);
gsqtrue[6]= Point3{Float64}(2.,4.,0.);
gsqtrue[7]= Point3{Float64}(4.,4.,0.);
gsqtrue[8]= Point3{Float64}(6.,4.,0.);
gsqtrue[9]= Point3{Float64}(8.,4.,0.);
gsqtrue[10]= Point3{Float64}(2.,6.,0.);
gsqtrue[11]= Point3{Float64}(4.,6.,0.);
gsqtrue[12]= Point3{Float64}(6.,6.,0.);
gsqtrue[13]= Point3{Float64}(4.,8.,0.);
#end test2 cylinder

#test cylinder
    R=2.;
    L=2.;
    D=4.;
    H=50.;
    m=2;
    gsq=cylinder(R,L,D,H,m);

    gsqtrue=fill(Point3{Float64}(0.,0.,0.),10,1);
    gsqtrue[1]= Point3{Float64}(2.,0.,4.);
    gsqtrue[3]= Point3{Float64}(0.,2.,4.);
    gsqtrue[5]= Point3{Float64}(2.,2.,4.);
    gsqtrue[7]= Point3{Float64}(4.,2.,4.);
    gsqtrue[9]= Point3{Float64}(4.,4.,4.);
    gsqtrue[2]= Point3{Float64}(2.,0.,29.);
    gsqtrue[4]= Point3{Float64}(0.,2.,29.);
    gsqtrue[6]= Point3{Float64}(2.,2.,29.);
    gsqtrue[8]= Point3{Float64}(4.,2.,29.);
    gsqtrue[10]= Point3{Float64}(4.,4.,29.);
#end test cylinder
