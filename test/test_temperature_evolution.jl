
steps = 25;
q = fill(1., steps)
field_characteristics = [0. 0. 4. 150.; 0.0 7.5 4.0 150; 0.0 15. 4.0 150; 7.5 0.0 4.0 150;7.5 7.5 4.0 150.; 7.5 15. 4.0 150.];
fls = FLS_III(field_characteristics, steps, cp = 3.1/1e-6/2300, rb = 0.075)[2]

dT = wall_temperature(q, fls)
