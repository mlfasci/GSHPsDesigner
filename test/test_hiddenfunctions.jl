"
Test fls(t::Array{T,1}, αg::T, δr::T, D1::T, D2::T, H1::T, H2::T)::Array{T,1} where {T<:AbstractFloat}
        The exact solution has been computed and visually compared with the solution given in Figure 4 in:
        Cimmino, M., Bernier, M., Adams, F., 2013. A contribution towards the determination of g-functions using the finite line source. Applied Thermal Engineering 51, 401–412. https://doi.org/10.1016/j.applthermaleng.2012.07.044
"
        αg = 1e-6;      # [m2/s]
        H = 100.;       # [m]
        rb = 0.05       # [m]
        D = 4.;         # [m]

        logt_ts = collect(-15:5:5)
        ts = H^2/9/αg;
        t = ts .*exp.(logt_ts)          # [s]

        gfunction = fls(t, αg, rb, D, D, H, H);
        exact = [0.03062111010029075
                1.9109396716065277
                4.376459677255315
                6.432485765233948
                6.716628920686025];

        @test maximum((gfunction .- exact) ./ exact) <= 1e-8;
" end Test fls(t::Array{T,1}, αg::T, δr::T, D1::T, D2::T, H1::T, H2::T)::Array{T,1} where {T<:AbstractFloat}"
