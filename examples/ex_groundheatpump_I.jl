# 1. Example of the function putting together SH and DHW
    using CSV
    using DataFrames

    n = 2;  # number of years to simlate
    H = 100.; # length of the borehole of interest
    borehole_extra_info = [1.; 0.; 0.]; # year of instaation and carteesian coordinates of the borehole of interest

    # Characteristics of the other boreholes: [year of installation, x coordinate, y coordinate, buried depth (use 6 if not available), borehole length, (length - buried depth)*8760/1000]
    field_characteristics = [1. 0. 0. 2. 152. 150*8760/1000;
                            1. 20. 0. 4. 184. 180*8760/1000;
                            1. 45. 0.0 6.0 306. 300*8760/1000;
                            1. 2. 25. 2.0 172. 170*8760/1000;
                            1. 5. 50. 1.0 201. 200*8760/1000;
                            1. 20. 40. 5.0 255. 250*8760/1000];

    df = CSV.read("data\\House_Data.csv", DataFrame)
    df.Profile = coalesce.(df.Profile, 0)
    df.T_cond_in = coalesce.(df.T_cond_in, 0)

    profile = df[!,"Profile"]
    Tcondin = df[!,"T_cond_in"]
    profile = convert(Array{Float64}, profile)
    Tcondin = convert(Array{Float64}, Tcondin) .+ 273.15

    # Simulation
    res = groundheatpump_I(n, field_characteristics, H, borehole_extra_info, profile, Tcondin);



    function groundheatpump_I(n_years, field_characteristics, H, borehole_extra_info, profile, Tcondin, rel_capacity)
