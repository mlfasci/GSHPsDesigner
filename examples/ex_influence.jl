# Example for radial_influence


D = 4.;     # borehole buried depth [m]
H = 100.;   # borehole length [m]
Q = 1500.; # borehole average yearly heat extraction [W]

q = Q/H;

t = 20. * 3600 * 8760;    # I want to know the influence after 20 years
dT_r = radial_influence(t, D, H, q)

r = 20.;
dT_t = temporal_influence(r, D, H, q)


plot(collect(1:300), dT_r)

plot(collect(1:100), dT_t)
