# Example 1
    field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 2136. / 242.242
    2001.0  68.7869  43.2332  4.99393  291.643 3335. / 291.643
    2003.0  96.8925  17.2583  6.78014  218.149 2157. / 218.149
    2004.0  37.8176  12.6237  6.99447  197.911 1986. / 197.911
    2006.0  51.5132  66.6104  5.41417  290.921 3489. / 290.921
    2007.0  73.5686  64.1044  7.22745  163.86 1820. / 163.86
    2009.0  78.1709  92.0879  4.41364  296.348 2876. / 296.348
    2012.0  37.0685  93.7397  7.88411  245.807 2809. / 245.807
    2018.0  65.0639  13.197   5.04953  158.442 1732. / 158.442];
    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 1498. / 175.695];
    tmax = 20;

    "1st case: the temperature threshold is overcome even without the new boreholes"
    dT_max = 5.71
    res = optimlength(field_characteristics, new_borehole_characteristics, tmax, dT_max)
    "2nd case: an insanely long borehol would be necessary to respect the temperature threshold"
    dT_max = 5.72
    optimlength(field_characteristics, new_borehole_characteristics, tmax, dT_max)
    "3rd case: it is possible to calculate an optimal length"
    dT_max = 5.74
    optimlength(field_characteristics, new_borehole_characteristics, tmax, dT_max)
    "4th case: the borehole length initially suggested is sufficientt to respect the temperature threshold"
    dT_max = 5.75
    optimlength(field_characteristics, new_borehole_characteristics, tmax, dT_max)
# End example 1


# Example 2
    field_characteristics = [2001.0  14.1406  26.5473  4.82105  242.242 2136. / 242.242
    2001.0  68.7869  43.2332  4.99393  291.643 3335. / 291.643
    2003.0  96.8925  17.2583  6.78014  218.149 2157. / 218.149];

    new_borehole_characteristics = [2020. 15.8655  97.6883  7.03478  175.695 1498. / 175.695];


    "Notice that the content of the first coloumn in the matrix new_field_characteristics is ignored by the algorithm.
    The new boreholes are assumed to be installed the same year as the newly planned borehole"
    new_field_characteristics = [2004.0  37.8176  12.6237  6.99447  197.911 1986. / 197.911
        2006.0  51.5132  66.6104  5.41417  290.921 3489. / 290.921
        2007.0  73.5686  64.1044  7.22745  163.86 1820. / 163.86
        2009.0  78.1709  92.0879  4.41364  296.348 2876. / 296.348
        2012.0  37.0685  93.7397  7.88411  245.807 2809. / 245.807
        2018.0  65.0639  13.197   5.04953  158.442 1732. / 158.442];

    tmax = 20;

    "1st case: the temperature threshold is overcome even without the new boreholes"
    dT_max = 3.
    res = optimlength(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, dT_max)
    "2nd case: an insanely long borehol would be necessary to respect the temperature threshold"
    dT_max = 4.2
    res = optimlength(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, dT_max)
    "3rd case: it is possible to calculate an optimal length"
    dT_max = 5.
    res = optimlength(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, dT_max)
    "4th case: the borehole length initially suggested is sufficientt to respect the temperature threshold"
    dT_max = 5.7
    res = optimlength(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, dT_max)
# End example 2
