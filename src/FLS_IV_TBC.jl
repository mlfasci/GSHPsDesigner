function dirichlet(ΔTsurf::T, t::T, αg::T, D::T, H::T) where{T<:AbstractFloat}
    f(x) =  x * (erf(-x/ sqrt(4*αg*t)) + sqrt(4*αg*t / (π*x^2)) * (1 - exp(-x^2/(4*αg*t))) + 1)
    ΔT = 1/ H * ΔTsurf * (f(H+D) - f(D))
end # function
"""
    FLS_IV_Dirichlet(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{T,2}, ΔTsurf::T, steps::Int64; rb::T = 5.75e-2, k::T = 3.1, ro::T = 2300., cp::T = 870., m::Int64 = 12,  stepsize::T = 8760. * 3600) where {T<:AbstractFloat}

Calculate the wall temperature change and heat extraction profile(s) of the borehole(s) wall due to both the borehole(s) heat extraction(s) and the non-homogeneous surface boundary condition (ground surface temperature being different from undisturded underground temperature).


# Outputs

1. `ΔT`: borehole(s) wall temperature change [K]
2. `q`: borehole(s)  extraction profile(s) [W/m]

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field [x position, y position, D: buried depth [m], H: net borehole length [m], linear borehole load (negative means extraction) [W/m]]
2. `ΔTsurf`: temperature change in the ground surface (positive means increase) [K]
3. `steps`: number of time steps to simulate [-]

## Keyword arguments
- `rb = 5.75e-2`: borehole radius [m]
- `k = 3.1`:  ground thermal conductivity [W/mK]

- `m = 12`: number of segments per borehole [-]
- `stepsize = 8760. * 3600`: time step size [s]
"""
function FLS_IV_Dirichlet(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{T,2}, ΔTsurf::T, steps::Int64; rb::T = 5.75e-2, k::T = 3.1, ro::T = 2300., cp::T = 870., m::Int64 = 12,  stepsize::T = 8760. * 3600) where {T<:AbstractFloat}
    if size(field_characteristics)[2] != 6 || size(new_borehole_characteristics)[2] != 6
        println("The matrices field_characteristics and new_borehole_characteristics should have 6 coloumns");
    elseif steps < 0
        println("steps must be a positive number");
    else
    αg = k/ ro/ cp;
    first_year = minimum(field_characteristics[:,1]);
    t = collect(1:steps+Int(new_borehole_characteristics[1] - first_year)).* stepsize; # [s] time at which the solutions are evaluated

    n_existing_boreholes = length(field_characteristics[:,1]);
    complete_field_characteristics = vcat(field_characteristics, new_borehole_characteristics);
    n_total_boreholes = length(complete_field_characteristics[:,1]);

    n_segments = n_total_boreholes * m;
    unique_years = Int64.(sort(unique(complete_field_characteristics[:,1])));
    n_unique_years= length(unique_years);
    complete_field_characteristics = hcat(complete_field_characteristics, collect(1:n_total_boreholes));
    ordered_field_characteristics = sortslices(complete_field_characteristics, dims = 1)
    distances = fill(0.,n_total_boreholes,n_total_boreholes);
    for ii = 1: n_total_boreholes
        for jj = 1:n_total_boreholes
            distances[ii,jj] = euclidean(ordered_field_characteristics[ii,2:3],ordered_field_characteristics[jj,2:3]);
        end
    end
    z_matrix = fill(0., n_total_boreholes, m)
    for ii = 1 : n_total_boreholes
        z_matrix[ii,:] = collect(ordered_field_characteristics[ii,4] : ordered_field_characteristics[ii,5]/m : ordered_field_characteristics[ii,4]+(m-1)*ordered_field_characteristics[ii,5]/m*100.1/100)  #*100.1/100 is added for computational reason
    end
    G3 = fill(Point3{Float64}(0.,0.,0.), n_segments);
    for ii = 1 : n_total_boreholes
        for jj = 1:m
            G3[(ii-1)*m+jj] = Point3{Float64}(ordered_field_characteristics[ii,2],ordered_field_characteristics[ii,3],z_matrix[ii,jj])
        end
    end
    h = fill(0., n_segments, n_segments, steps+Int(new_borehole_characteristics[1] - first_year));
    for ii = 1 : n_segments
        ii_subindex = Int64(ceil(ii/m));
        for jj = 1 : n_segments
            jj_subindex = Int64(ceil(jj/m));
            h[ii,jj,:] = fls(t, αg, max(distances[ii_subindex,jj_subindex],rb), G3[jj][3], G3[ii][3], ordered_field_characteristics[Int64(ceil(jj/m)),5]/m, ordered_field_characteristics[Int64(ceil(ii/m)),5]/m) ./ (2*pi*k);
        end
    end
	## Calculating the underground temperature due to the surface boundary condition
		dT_Dirichlet = fill(0., n_total_boreholes, m,steps)
		for ii = 1 : n_total_boreholes
			for kk = 1 : m
				for jj = 1 : steps
					dT_Dirichlet[ii,kk,jj] = abs.(dirichlet(ΔTsurf, t[jj], αg, z_matrix[ii,kk], ordered_field_characteristics[ii,5]/m));
				end
			end
		end
		b_Dirichlet = fill(0., n_segments, steps)
		for ii = 1 : steps
			b_Dirichlet[:,ii] = reshape(transpose(dT_Dirichlet[:,:,ii]),(:,1))
		end
	##
	# Building matrix A_____________________________________________________________
		A1 = fill(0., n_total_boreholes, n_total_boreholes);
		A2 = fill(0., n_total_boreholes, n_segments);
		A3 = fill(0., n_segments, n_total_boreholes);
		for ii = 1 : n_total_boreholes
			for jj = (ii-1) * m + 1: (ii) * m
				A2[ii,jj] = ordered_field_characteristics[ii,5]/m;        # construction matrix A2
				A3[jj,ii] = -1;          # construction matrix A3
			end
		end
		A4 = h[:,:,1]
		AA = vcat(hcat(A1, A2),hcat(A3,A4));
		A_inv= inv(AA);
	# end (Building matrix A)_____________________________________________________
	# Building b0_______________________________________________________________
		qtot_boreholes = ordered_field_characteristics[:,6] .* 1e3/8760;
		b01 = fill(0.,n_total_boreholes, steps);
		b01[:,1] = qtot_boreholes[1:n_total_boreholes,1];
		for ii = 2 : steps
			b01[:,ii] = qtot_boreholes[1:n_total_boreholes,1];
		end
		b0 = vcat(b01[:,1], fill(0., n_segments) .- b_Dirichlet[:,1]);
	# end (Building b0)_________________________________________________________
	# Initialization of the solution vector_________________________________________
		x = fill(0., n_total_boreholes + n_segments, steps);
	# end (Initialization of the solution vector)___________________________________
	# Solution at the first time step_______________________________________________
		x[:,1] = A_inv * b0;
	#end (Solution at the first time step)
	# Solution
		for TT = 2 : steps
	        convolution = fill(0., n_segments, 1);
	        for kk = 1 : TT-1
	            convolution = convolution .+ (h[:,:,TT-kk]-h[:,:,TT-(kk-1)]) * x[n_total_boreholes + 1 : end, kk]
	        end
	        b = vcat(b01[:,TT], + convolution .- b_Dirichlet[:,TT]);
	        x[:,TT] = A_inv * b;
	    end
	#
	ΔT = x[1 : n_total_boreholes,:];
	q = x[n_total_boreholes + 1 : end,:];
	return ordered_field_characteristics, ΔT, q
end
end
"""
    FLS_IV_FFT(D::T, H::T, steps::Int; rb::T=0.0575, k::T = 3.1, ro::T = 2300., c_p::T = 870., step_res::T = 3600., m::Int=12) where {T<:AbstractFloat}

Calculate the self-influence of a borehole using the FLS model with BC IV: uniform temeprature. It assumes 1 W/m.

IT IS THE MODEL PROPOSED IN THE ARTICLE: "???"! It works only for the self-response of a borehole
It usese the Fast Fourier Transform (FFT) to solve the convolution product.
# Output:

`temperature_change`: DIMENSIONAL thermal response [K]

# Arguments:
1. `D`: buried depth                            [m]
2. `H`: length of the boreholes                 [m]
3. `steps`: number of time steps to simulate    [-]

# Keyword Arguments:

 - `k = 3.1`:  ground thermal conductivity      [W/mK]
 - `ro = 2300.`: ground density                  [kg/m3]
 - `cp = 870.`: ground heating capacity          [J/(kg K)]
 - `rb = 5.75e-2`: borehole radius               [m]
 - `m = 12`: number of segments per borehole    [-]
 - `step_res`: length of a time step             [-]
"""
function FLS_IV_FFT(D::T, H::T, steps::Int, ΔTsurf::T; rb::T = 5.75e-2, k::T = 3.1, ro::T = 2300., cp::T = 870., step_res::T = 3600., m::Int=12) where {T<:AbstractFloat}
    αg = k / ro / cp;
    t = collect(1:steps) .* step_res;

    Hseg = H/m;
    z_vector = D : Hseg : D + (m-1) * Hseg *100.1 / 100;

    h = fill(0., m, m, steps);
    h_FFT = fill(0. + 0 * im, m, m, steps*2);
    for ii = 1 : m            ## ii points to the affected borehole
        for jj = 1 : m        ## jj points to the heat source borehole
            h[ii,jj,:] = fls(t, αg, rb, z_vector[jj], z_vector[ii], Hseg, Hseg);
            h_FFT[ii,jj,:] = (fft(vcat(h[ii,jj,:], fill(0.,steps))));
        end
    end
    ## Calculating the underground temperature due to the surface boundary condition
	b_Dirichlet = fill(0., m, steps)
    b_FFT = fill(0. + 0 * im, m, steps*2);
    for kk = 1 : m
		for jj = 1 : steps
			b_Dirichlet[kk,jj] = abs.(dirichlet(ΔTsurf, t[jj], αg, z_vector[kk], Hseg));
		end
        b_FFT[kk,:] = fft(vcat(b_Dirichlet[kk,:], fill(0.,steps)))
	end


    qtot = fill(0., steps * 2);

    qtot[1] = H
    q_FFT = (fft(vcat(qtot, fill(0., steps))));

    x = fill(0. + 0 * im, 1 + m, steps*2);
    bb = fill(0. + 0 * im, 1 + m);

    A1 = fill(0., 1, 1);
    A2 = fill(Hseg, 1, m);
    A3 = fill(-1, m, 1);
    A4 = fill(0. + im, m, m);
    Aup = hcat(A1 , A2);
    for kk = 1 : steps*2
        A4[1 : m , 1 : m] = h_FFT[1 : m , 1 : m , kk];
        AA = vcat(Aup , hcat(A3,A4));
        bb[1] = q_FFT[kk];
        bb[2:end] = b_FFT[:,kk]
        x[:,kk] = AA\bb;
    end

    y = fill(0. + im, 1 + m, steps*2)
    for ii = 1 : 1 + m
        # y[ii,:] = real.(expst .* ifft(fftshift(x[ii,:])));
        # y[ii,:] = real.(ifft(fftshift(x[ii,:])));
        y[ii,:] = real.(ifft(x[ii,:]));
    end
    temperature_change = Real.(y)[1,1:steps] ./ (2*pi*k)
	return temperature_change
end
