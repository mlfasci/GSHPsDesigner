"
function ierf(x)
	used in 'integrand_fls'
	A definition of ierf(x) can be found in ''??? - Article to be published'.

	It requires the package SpecialFunctions, containing the error function 'erf'
"
function ierf(x)
	return x*erf(x)-1/sqrt(pi)*(1-exp(-x^2))
end

"
function integrand_fls(u, δr, D1, D2, H1, H2)
	integrand of the function to calculate the thermal response function between two borehole segments assuming constant heat flux boundary condition.
	OBS! (for who is familiar with the problem) This function takes into account both the source segment and image segment.

	u: integration dummy variable [-]

	δr: radial distance between the segments [m]
	D1: buried depth of the active borehole segment [m]
	D2: buried depth of the borehole segment for which the response is calculated [m]
	H1: length of the active borehole segment [m]
	H2: length of the borehole segment for which the response is calcualted [m]
"

function integrand_fls(u::T, δr::T, D1::T, D2::T, H1::T, H2::T) where {T<:AbstractFloat}
	return 1/u^2*exp(-δr^2*u^2)*(ierf((D2-D1+H2)*u) - ierf((D2-D1)*u) + ierf((D2-D1-H1)*u) - ierf((D2-D1+ H2-H1)*u) + ierf((D2+D1+H2)*u) - ierf((D2+D1)*u) + ierf((D2+D1+H1)*u) - ierf((D2+D1+H2+H1)*u))
end

"
	function fls(t::Array{T,1}, αg::T, δr::T, D1::T, D2::T, H1::T, H2::T)::Array{T,1} where {T<:AbstractFloat}

	t: times at which the response functions must be calcualted [s]
	αg: ground thermal diffusivity [m2/s]
	δr: radial distance between the segments [m]
	D1: buried depth of the active borehole segment [m]
	D2: buried depth of the borehole segment for which the response is calculated [m]
	H1: length of the active borehole segment [m]
	H2: length of the borehole segment for which the response is calcualted [m]

	It requires the package QuadGK for the function 'quadgk'.
"
function fls(t::Array{T,1}, αg::T, δr::T, D1::T, D2::T, H1::T, H2::T)::Array{T,1} where {T<:AbstractFloat}

"
 The integral should have Inf as upper boundary. This leads to slower computational time. The following lines select an appropriate finite upper boundary for the integral.
 For more info read Appendix A in: FascÌ, Maria Letizia, et al. 'Analysis of the thermal interference between ground source heat pump systems in dense neighborhoods.' Science and Technology for the Built Environment 25.8 (2019): 1069-1080.
"
	f(u)=(1/u^2*exp(-δr^2*u^2)*(ierf((D2-D1+H2)*u) - ierf((D2-D1)*u) + ierf((D2-D1-H1)*u) - ierf((D2-D1+ H2-H1)*u) + ierf((D2+D1+H2)*u) - ierf((D2+D1)*u) + ierf((D2+D1+H1)*u) - ierf((D2+D1+H2+H1)*u)))-1e-16;
	tip= sqrt(6. / δr^2);
	upb = fill(0.,length(t));
	upb[1] = find_zero(f,tip, atol = 1e-16);
" End of the lines to select a finite upper boundary "
	lb = 1 ./ sqrt.(4 .* αg .* t);
	for ii = 2:length(t)
		upb[ii] = lb[ii-1];
	end
	partial_integrals = fill(0.,length(t));
	for ii = 1: length(t)
		partial_integrals[ii] = 1/2/H2 * quadgk(u->integrand_fls(u, δr, D1, D2, H1, H2), lb[ii], upb[ii], atol = 1e-8)[1];
	end
	return cumsum(partial_integrals);
end

function fls(t::T, αg::T, δr::Array{T,1}, D1::T, D2::T, H1::T, H2::T)::Array{T,1} where {T<:AbstractFloat}
"
 The integral should have Inf as upper boundary. This leads to slower computational time. The following lines select an appropriate finite upper boundary for the integral.
 For more info read Appendix A in: FascÌ, Maria Letizia, et al. 'Analysis of the thermal interference between ground source heat pump systems in dense neighborhoods.' Science and Technology for the Built Environment 25.8 (2019): 1069-1080.
"
integral = fill(0.,length(δr));
for ii = 1: length(δr)
	f(u)=(1/u^2*exp(-δr[ii]^2*u^2)*(ierf((D2-D1+H2)*u) - ierf((D2-D1)*u) + ierf((D2-D1-H1)*u) - ierf((D2-D1+ H2-H1)*u) + ierf((D2+D1+H2)*u) - ierf((D2+D1)*u) + ierf((D2+D1+H1)*u) - ierf((D2+D1+H2+H1)*u)))-1e-16;
	tip= sqrt(6. / δr[ii]^2);
	upb = find_zero(f,tip, atol = 1e-16);
" End of the lines to select a finite upper boundary "
	lb = 1 / sqrt(4 * αg * t)
	integral[ii] = 1/2/H2 * quadgk(u->integrand_fls(u, δr[ii], D1, D2, H1, H2), lb, upb, atol = 1e-8)[1];
end
return integral
end
