module GSHPsDesigner

using CSV
using CoolProp
using DataFrames
using DelimitedFiles
using Distances
using FFTW
using GeometryTypes
using GLM
using JLD
using Printf
using Optim
using QuadGK
using Roots
using SpecialFunctions
using Suppressor
using XLSX

include("hiddenfunctions.jl")
include("FLS_I.jl")
    export FLS_I
    export(fls)
include("FLS_IV.jl")
    export FLS_IV
    export FLS_IV_FFT
include("FLS_IV_TBC.jl")
    export FLS_IV_Dirichlet
include("FLS_III.jl")
    export FLS_III
    export FLS_III_FFT
include("temperature_evolution.jl")
    export wall_temperature
    export brine_temperature
include("grids.jl")
include("influence.jl")
    export radial_influence
    export temporal_influence
include("minimumlength.jl")
    export minimumlength_FLS_I
include("sustainabletime.jl")
    export sustainabletime
include("minimumtemperature.jl")
    export temperature_penalty
    export minimum_temperature
    export fluid_temperature
include("heatpump.jl")
    export generate_regressions
    export predict_performance
    export hp_onoff
include("house.jl")
    export house_heating_demand
    export profile_from_file
    export relative_hp_capacity
    export hp_capacity
include("groundheatpump_I.jl")
    export groundheatpump_I
    export groundheatpump_I_as
include("groundheatpump_IV.jl")
    export groundheatpump_IV
    export groundheatpump_IV_as
include("groundPVheatpump.jl")
    export groundPVheatpump
include("load_aggregation.jl")


end #module
