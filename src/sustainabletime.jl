"""
    sustainabletime(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}

Calculate the year after which the underground temperature change will exceed the imposed threshold.

# Arguments

- field_characteristics: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- new_borehole_characteristics: matrix containing the following characteristics for the planned borehole: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- dT_max: maximum temperature change allowed [K]

## Optional arguments
- k = 3.1;       ground thermal conductivity [W/mK]
- ro = 2300;     ground density [kg/m3]
- cp = 870;      ground heating capacity [J/(kg K)]
- rb = 7.5e-2;   borehole radius [m]

"""
function sustainabletime(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}
    dTs(tmax) = maximum(FLS_I(field_characteristics, new_borehole_characteristics, max(Int64(floor(tmax)), 1), k=k, ro=ro, cp=cp, rb=rb)[1])
    objective_function(tmax) =  abs.(dTs(tmax) - dT_max)

    time = optimize(objective_function, 0, 60)

    if time.minimizer > 50
        @printf("This neighbourhoood will operate sustainably for at least 50 years")
    else
        @printf("This neighbourhood will operate sustainably for %2.0f years", Int64(floor(time.minimizer)))
    end
end
function sustainabletime(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, new_field_characteristics::Array{Float64,2}, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}
    dTs(tmax) = maximum(FLS_I(field_characteristics, new_borehole_characteristics, new_field_characteristics, max(Int64(floor(tmax)), 1), k=k, ro=ro, cp=cp, rb=rb)[4])
    objective_function(tmax) =  abs.(dTs(tmax) - dT_max)

    time = optimize(objective_function, 0, 60)

    if time.minimizer > 50
        @printf("This neighbourhoood will operate sustainably for at least 50 years")
    else
        @printf("This neighbourhood will operate sustainably for %2.0f years", Int64(floor(time.minimizer)))
    end
end
