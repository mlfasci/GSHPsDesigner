"""
    generate_regressions(df_file::String)

## Create the regression models for the Heating Capacity and COP of a Heat Pump given the manufacturer data.

## Output:

1. `regrHC`: regression model for the heating capacity [W]
2. `regrCOP`: regression model for the COP [-]

## Arguments:
1. `df_file`: absolute path to the CSV file containing the dataframe with HP data

The file must be formatting in the following way:

    v_s2, t_is_v_s, t_is_v_l , v_s_v_l , t_il_v_l , t_il2 , t_is_t_il , v_s_t_il, t_is, v_s, t_il, v_l, HC, COP

where:

- `v_s`: volumentric flow rate of the water in the evaporator [l/s]
- `t_is`: temperature of the secondary fluid entering the evaporator [°C]
- `v_l`: volumentric flow rate of the water in the condenser [l/s]
- `t_il`: temperature of the water entering the condenser [°C]
- `v_s2` = v_s^2
- `t_is_v_s` = t_is * v_s
- etc...
- `HC`: heating capacity [W]
- `COP`: coefficient of performance [-]

### Reference:
    Simon, F., et al. 2016. Developing multiple regression models from the manufacturer’s ground-source heat pump catalogue data.
    Renewable Energy 95, 413–421. https://doi.org/10.1016/j.renene.2016.04.045

"""
function generate_regressions(df_file::String)
    df = DataFrame(CSV.File(df_file))

    regrHC = lm(@formula(HC ~ v_s2 + t_is_v_s + t_is_v_l + v_s_v_l + t_il_v_l + t_is + t_il + v_l ), df)
    regrCOP = lm(@formula(COP ~ t_il2 + t_is_v_s + t_is_t_il + t_is_v_l + v_s_t_il + t_il_v_l + t_il + v_l), df)

    return regrHC, regrCOP
end
"""
    predict_performance(v_s, v_l, t_is, t_il, regrHC, regrCOP)

# Calculate the heating capacity and COP of a Heat Pump with its regression models.

# Output:
1. `HC`: heating capacity [W]
2. `COP`: COP [-]

# Arguments:

1. `v_s`: volumetric flow rate of the secondary fluid in the evaporator [l/s]
2. `v_l`: volumetric flow rate of the secondary fluid in the condenser [l/s]
3. `t_is`: temperature of the secondary fluid at the inlet of the evaporator [°C]
4. `t_il`: temperature of the secondary fluid at the inlet of the condenser [°C]
5. `regrHC`: regression model to calculate the heating capacity [W]
6. `regrCOP`: regression model to calculate the COP [-]

### Reference:
    Simon, F., et al. 2016. Developing multiple regression models from the manufacturer’s ground-source heat pump catalogue data.
    Renewable Energy 95, 413–421. https://doi.org/10.1016/j.renene.2016.04.045
"""
function predict_performance(v_s::T, v_l::T, t_is::T, t_il::T, regrHC, regrCOP) where{T<:AbstractFloat}
    df = DataFrame(v_s2 = v_s^2, t_is_v_s = t_is*v_s, t_is_v_l = t_is * v_l, v_s_v_l = v_s*v_l, t_il_v_l = t_il*v_l, t_is = t_is, t_il = t_il, v_l = v_l)
    HC = predict(regrHC, df)
    df = DataFrame(t_il2 = t_il^2, t_is_v_s = t_is * v_s, t_is_t_il = t_is * t_il, t_is_v_l = t_is * v_l, v_s_t_il = v_s * t_il, t_il_v_l = t_il * v_l, t_il = t_il, v_l = v_l)
    COP = predict(regrCOP, df)

    return HC, COP
end
"""
   hp_onoff(Qcond::T, Qeva::T, W::T, Qload::T) where {T <:AbstractFloat}
       Qcond: heating capacity [W]
       Qeva:  cooling capacity [W]
       W:     compressor power [W]

       Qload: heating load [W]

 Returns: 1. the share of the hour when the HP was turned on, ontime; 2. The energy extracted from the heat source, extraction; 3. The electricity consumed by the compressor, compressorenergy, 4. The electricity required by the auxiliary heating, Q_aux.
"""
function hp_onoff(Qcond::T, Qeva::T, W::T, Qload::T, ontime_allowed::T) where {T <:AbstractFloat}
   ontime = Qload / Qcond;                      # share of the time when the HP is on [-]
   if ontime <= ontime_allowed
       extraction = Qeva * ontime;
       compressorenergy = W * ontime;
       Q_aux = 0.;
   else                                         # the house requires auxiliary heating
       Q_aux = Qload - Qcond * ontime_allowed;
       extraction = Qeva * ontime_allowed;
       compressorenergy = W * ontime_allowed;
       ontime = ontime_allowed;
   end
   return [ontime, extraction, compressorenergy, Q_aux]
end
