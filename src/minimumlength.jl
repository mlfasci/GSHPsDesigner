"""
    minimumlength_FLS_I(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}

Calculate the minimum borehole length that guarantees the respect of the imposed temperature change threshold.

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `tmax`: final timestep at which to calculate the temperature [year]
4. `dT_max`: maximum temperature change allowed [K]

## Optional arguments
- `T_ground = 8`   initial ground temperature [°C]
- `k = 3.1`;       ground thermal conductivity [W/mK]
- `ro = 2300`;     ground density [kg/m3]
- `cp = 870`;      ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;   borehole radius [m]
"""
function minimumlength_FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, tmax::Int, dT_max::T; T_ground = 8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}
    temperatures = FLS_I(field_characteristics, new_borehole_characteristics, tmax, k=k, ro=ro, cp=cp, rb=rb)
    temperature_developments_current =  T_ground .- temperatures[2]
    temperature_developments_future = T_ground .- temperatures[1]
    current_max_dT = maximum(temperature_developments_current)
    future_max_dT =  maximum(temperature_developments_future)

    if current_max_dT >= dT_max
            @printf("\n\nOBS! The underground temperature change will exceed the threshold even without the current installation\n\nThe maximum temperature change without the new installation will be: %0.2f degrees\n\nThe maximum temperature change with the new installation will be: %0.2f degrees.\n", current_max_dT, future_max_dT)
            required_length = -1.;
            new_borehole_characteristics[5] = -1;
            required_new_borehole_characteristics = new_borehole_characteristics;
    elseif future_max_dT <= dT_max
            @printf("\n\nCongrats! The proposed borehole length is sufficient to respect the temperature threshold\n\nThe maximum temperature change without the new installation will be: %0.2f degrees\n\nThe maximum temperature change with the new installation will be: %0.2f degrees.\n", current_max_dT, future_max_dT)
            required_length = new_borehole_characteristics[5];
            required_new_borehole_characteristics = new_borehole_characteristics;
    else
        max_dT_field(x) = maximum(T_ground .- FLS_I(field_characteristics, updatecharacteristics(new_borehole_characteristics, x), tmax, k=k, ro=ro, cp=cp, rb=rb)[1]);
        objective_function(x) = abs(max_dT_field(x) - dT_max);

        lower_bound = new_borehole_characteristics[5] - new_borehole_characteristics[4];
        upper_bound = 5 * lower_bound;
        optimized_length = optimize(objective_function, lower_bound, upper_bound)
        required_length = Optim.minimizer(optimized_length) + new_borehole_characteristics[4];

        res = current_max_dT, future_max_dT, required_length;
            @printf("\n\nThe proposed borehole length is not sufficient to respect the imposed temperature threshold. The maximum temperature change with the new proposed installation would be: %0.2f degrees.\n\nThe required borehole length to respect the threshold is: %.0f m \n\n", future_max_dT, required_length)
        required_new_borehole_characteristics = updatecharacteristics(new_borehole_characteristics, required_length)
    end

    res = current_max_dT, future_max_dT, required_length;
    return res, required_new_borehole_characteristics
end
"""
    minimumlength_FLS_I(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, new_field_characteristics::Array{Float64,2}, tmax::Int, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}

Calculate the minimum borehole length that guarantees the respect of the imposed temperature change threshold taking into account also possible future installations.

# Arguments

1. `field_characteristics`: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, x position, y position, D: buried depth [m],H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
2. `new_borehole_characteristics`: matrix containing the following characteristics for the planned borehole: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
3. `new_field_characteristics`: matrix containing on each row the following characteristics for 1 possible future borehole in the field: [year of installation, x position, y position, D: buried depth [m], H: total borehole length [m] , yearly borehole load (extraction) [kWh]]
    ** this value will actually be ignored as it is assumed that all the future boreholes wll be installed at the same time as the borehole under design
4. `tmax`: final timestep at which to calculate the temperature [year]
5. `dT_max`: maximum temperature change allowed [K]

## Optional arguments
- `k = 3.1`;       ground thermal conductivity [W/mK]
- `ro = 2300`;     ground density [kg/m3]
- `cp = 870`;      ground heating capacity [J/(kg K)]
- `rb = 7.5e-2`;   borehole radius [m]
"""
function minimumlength_FLS_I(field_characteristics::Array{T,2}, new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2}, tmax::Int, dT_max::T; T_ground = 8., k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}
    temperatures = FLS_I(field_characteristics, new_borehole_characteristics, new_field_characteristics, tmax, k=k, ro=ro, cp=cp, rb=rb)
    temperature_developments_current =  T_ground .- temperatures[2]
    temperature_developments_future = T_ground .- temperatures[4]
    current_max_dT = maximum(temperature_developments_current)
    future_max_dT =  maximum(temperature_developments_future)
    if current_max_dT >= dT_max
        @printf("\n\nOBS! The underground temperature change will exceed the threshold even without the current installation(s)\n\nThe maximum temperature change without the new installation will be: %0.2f degrees\n\nThe maximum temperature change with the new installation will be: %0.2f degrees.", current_max_dT, future_max_dT)
        required_length = -1.;
        new_borehole_characteristics[5] = -1;
        new_field_characteristics[:,5] .= -1;
        required_new_borehole_characteristics = new_borehole_characteristics;
        required_new_field_characteristics = new_field_characteristics;
    elseif future_max_dT <= dT_max
        @printf("\n\nCongrats! The proposed borehole length is sufficient to respect the temperature threshold\n\nThe maximum temperature change without the new installation(s) will be: %0.2f degrees\n\nThe maximum temperature change with the new installation(s) will be: %0.2f degrees.", current_max_dT, future_max_dT)
        required_length = new_borehole_characteristics[5];
        required_new_borehole_characteristics = new_borehole_characteristics;
        required_new_field_characteristics = new_field_characteristics;
    else
        maxdT_field(x) = maximum(T_ground .- FLS_I(field_characteristics, updatecharacteristics(new_borehole_characteristics, x), updatecharacteristics(new_borehole_characteristics, new_field_characteristics, x), tmax, k=k, ro=ro, cp=cp, rb=rb)[4]);
        objective_function(x) = abs(maxdT_field(x) - dT_max);

        lower_bound = new_borehole_characteristics[5] - new_borehole_characteristics[4];
        upper_bound = 10 * lower_bound;
        optimized_length = optimize(objective_function, lower_bound, upper_bound)
        required_length = Optim.minimizer(optimized_length) + new_borehole_characteristics[4];
        required_new_borehole_characteristics = updatecharacteristics(new_borehole_characteristics, required_length)
        required_new_field_characteristics = updatecharacteristics(new_borehole_characteristics, new_field_characteristics, required_length)

        @printf("\n\nThe proposed borehole length is not sufficient to respect the imposed temperature threshold. The maximum temperature change with the new proposed installation(s) would be: %0.2f degrees. Without the installation(s) it would be %0.2f degrees.\n\n The required borehole length to respect the threshold is: %.0f m \n\n", future_max_dT, current_max_dT, required_length)
    end

    res = current_max_dT, future_max_dT, required_length;
    return res, required_new_borehole_characteristics, required_new_field_characteristics
end

function updatecharacteristics(new_borehole_characteristics::Matrix{T}, newlength::T) where {T<:AbstractFloat}
    new_characteristics = copy(new_borehole_characteristics);
    new_characteristics[5] = newlength + new_borehole_characteristics[4];
    return new_characteristics
end

function updatecharacteristics(new_borehole_characteristics::Array{T,2}, new_field_characteristics::Array{T,2}, newlength::T) where {T<:AbstractFloat}
    new_bh_characteristics = copy(new_borehole_characteristics);
    new_fi_characteristics = copy(new_field_characteristics);
    length_ratio = (new_borehole_characteristics[5]- new_borehole_characteristics[4]) / newlength;
    new_bh_characteristics[5] = newlength + new_borehole_characteristics[4];

    new_fi_characteristics[:,5] = new_field_characteristics[:,4] .+ (new_field_characteristics[:,5].-new_field_characteristics[:,4]) ./ length_ratio;
    return new_fi_characteristics
end
"""
    minimumlength(field_characteristics::Array{Float64,2}, new_borehole_characteristics::Array{Float64,2}, tmax::Int, dT_max::T, k::T=3.1, ro::T=2300., cp::T=870., rb::T=7.5e-2) where {T<:AbstractFloat}

Calculate the minimum borehole length that guarantees the respect of the imposed temperature change threshold.

# Arguments

- field_characteristics: matrix containing on each row the following characteristics for 1 borehole in the field: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- new_borehole_characteristics: matrix containing the following characteristics for the planned borehole: [year of installation, xpos, ypos, D, H, linear load [W/m]]
- tmax: final timestep at which to calculate the temperature [year]
- dT_max: maximum temperature change allowed [K]

## Optional arguments
- k = 3.1;       ground thermal conductivity [W/mK]
- ro = 2300;     ground density [kg/m3]
- cp = 870;      ground heating capacity [J/(kg K)]
- rb = 7.5e-2;   borehole radius [m]

"""
function minimumlength_FLS_I(file_name::String) where{T<:AbstractFloat}
## Load the excel file with input data. The file must be in the folder Projects. Julia must be activated in the folder GSHPsDesigner
    access_file = XLSX.readxlsx("Projects/"*file_name)

## Check that the user specified the number of time steps to simulate
    steps = access_file["Other input"]["E2"]
    if typeof(steps) <: Missing
        println("\nYou have not specified how many time steps you want to simulate. Please, fill cell 'E2' in the 'Other input' sheet of the excel file \n")
        return
    end

    dT_max = access_file["Other input"]["C2"]
## Extrapolate the ground and borehole field_characteristics
    UndisturbedT = access_file["Other input"]["A2"]
    conductivity = access_file["Other input"]["A6"]
    density = access_file["Other input"]["A9"]
    specificheat = access_file["Other input"]["A12"]
    boreholeradius = access_file["Other input"]["A15"]

## Extrapolate the matrix "field_characteristics", fill the missing values and convert it to the right format
    n_ex = access_file["Existing installations"]["H2"]
    interval = string("A2:F",n_ex + 1)
    field_characteristics = access_file["Existing installations"][interval]
    for ii = 1 : n_ex
        if typeof(field_characteristics[ii,4]) <: Missing
            field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(field_characteristics[ii,6]) <: Missing
            field_characteristics[ii,6] = field_characteristics[ii,5] * 15 * 8760/1000 ; # 15 W/m is used as a default linear heat extraction load
        end
    end
    field_characteristics = convert(Array{AbstractFloat,2}, field_characteristics)

## Extrapolate the matrix "new_borehole_characteristics", fill the missing values and convert it to the right format
    new_borehole_characteristics = access_file["New borehole"]["A2:F2"]
        if typeof(new_borehole_characteristics[4]) <: Missing
            new_borehole_characteristics[4] = 6.; # 6 m is used as a default buried depth
        end
        if typeof(new_borehole_characteristics[6]) <: Missing
            new_borehole_characteristics[6] = new_borehole_characteristics[5] * 15 * 8760/1000; # 15 W/m is used as a default linear heat extraction load
        end
        new_borehole_characteristics = convert(Array{AbstractFloat,2}, new_borehole_characteristics)

## Extrapolate the matrix "new_field_characteristics" (if existing), fill the missing values and convert it to the right format
    n_f = access_file["Future installations"]["H2"]
    if n_f > 0
        interval = string("A2:F", n_f + 1)
        new_field_characteristics = access_file["Future installations"][interval]
        for ii = 1 : n_f
            new_field_characteristics[ii,1] = new_borehole_characteristics[1];
            if typeof(new_field_characteristics[ii,4]) <: Missing
                new_field_characteristics[ii,4] = 6.; # 6 m is used as a default buried depth
            end
            if typeof(new_field_characteristics[ii,6]) <: Missing
                new_field_characteristics[ii,6] = new_field_characteristics[ii,5] * 15 * 8760/1000 ; # 15 W/m is used as a default linear heat extraction load
            end
        end
        new_field_characteristics = convert(Array{AbstractFloat,2}, new_field_characteristics)

        res = minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, new_field_characteristics, steps, dT_max, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius);
    else
        res = minimumlength_FLS_I(field_characteristics, new_borehole_characteristics, steps, dT_max, T_ground = UndisturbedT, k = conductivity, ro = density, cp = specificheat, rb = boreholeradius);
    end

## Export the results on a new excel file
    XLSX.openxlsx("Projects/Results_minimumlength_FLS_I_"*file_name, mode="w") do xf
    # XLSX.rename!(xf[1], " ") Use this line to rename the excel sheet
    xf[1]["A1"] = "Proposed length [m]"
    xf[1]["A4"] = "Required length [m]"
    xf[1]["A7"] = "Maximum temperature change without the proposed borehole(s) [°C]"
    xf[1]["A10"] = "Maximum temperature change with the proposed borehole(s) length(s) [°C]"
    xf[1]["A13"] = "Maximum temperature change imposed [°C]"

    xf[1]["A2"] = new_borehole_characteristics[5]
    xf[1]["A8"] = res[1][1];
    xf[1]["A11"] = res[1][2];
    xf[1]["A14"] = dT_max;
    if res[1][3] == 0.
        xf[1]["A5"] = "The proposed length is ok"

    elseif res[1][3] == -1.
        xf[1]["A5"] = "The temperature threshold is already exceeded due to the existing installations"
    else
        xf[1]["A5"] = res[1][3];
    end
end
if n_f >0
    return res[2], res[3]
else
    return res[2]
end
end
