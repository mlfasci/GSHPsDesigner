"""
    wall_temperature(q::Array{T,1}, fls::Array{T,1})::T where {T<:AbstractFloat}

Calculate the wall temperature change of a borehole (field). It does not take into account neighbouring installations.

# Outputs

1.  `dT`: temperature change [K]

# Arguments

1. `q`: linear borehole (field) load [W/m]
2. `fls`: thermal response of the borehole (field) to a unitary heat extraction load [m*K/W]

"""
function wall_temperature(q::Array{T,1}, fls::Array{T,1})::T where {T<:AbstractFloat}
       nsteps = length(q);
       sum = 0.;
       for kk = 1: nsteps-1
               sum = sum + q[kk] * ( fls[(nsteps) - kk] - fls[(nsteps) - kk + 1]);
       end
       dT = -sum + q[end] * fls[1];
       return dT
end
"""
    brine_temperature(q::Array{T,1}, H::T, fls::Array{T,1}, flow_rate; T0 = 8. + 273.15, Rb::T=0.15, Brine="INCOMP::MEA-25%")::T where {T<:AbstractFloat}

Calculate the brine temperature after the borehole heat exchanger.

# Outputs

1.  `T_brine`: brine temperature [K]

# Arguments

1. `q`: linear borehole (field) load [W/m]
2. `H`: borehole (field) length [m]
3. `fls`: thermal repsonse of the borehole (field) to a unitary heat extraction load [m*K/W]
4. `flow_rate`: total mass flow rate of the brine [kg/s]

## Optional Arguments

`T0`: undisturbed ground temperature [K]
`Rb`: borehole resistance [m*K/W]
`Brine`: Name of the brine as used in CoolProp
"""
function brine_temperature(q::Array{T,1}, H::T, fls::Array{T,1}, flow_rate::T; T0 = 8. + 273.15, Rb::T=0.07, Brine="INCOMP::MEA-28%")::T where {T<:AbstractFloat}
    dT = wall_temperature(q, fls)
    T_bh = T0 - dT;
    ObjectiveFunction(x) = abs((T_bh - q[end] * Rb + q[end] * H/2/flow_rate/ CoolProp.PropsSI("C", "P", 101325., "T", x, Brine)) - x)
    T_brine = optimize(ObjectiveFunction, T_bh-10, T_bh).minimizer;

    return dT, T_brine
end
"""
Add description
"""
function brine_temperature(T_bh::T, q::Array{T,1}, H::T, flow_rate::T; T0 = 8. + 273.15, Rb::T=0.07, Brine="INCOMP::MEA-28%")::T where {T<:AbstractFloat}
    ObjectiveFunction(x) = abs((T_bh - q[end] * Rb + q[end] * H/2/flow_rate/ CoolProp.PropsSI("C", "P", 101325., "T", x, Brine)) - x)
    T_brine = optimize(ObjectiveFunction, T_bh-10, T_bh).minimizer;

    return T_brine
end
