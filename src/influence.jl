"""
    radial_influence(t::T, D::T, H::T, q::T)::Array{T,1} where {T<:AbstractFloat}

Calculate the ground temperature change due to the operation of the borehole considered at a given time as a function of the distance from the borehole

# Arguments

- t: time at which the influence is calculated [s]
- D: borehole buried depth [m]
- H: borehole length [m]
- q: borehole yearly extraction load [W]
"""
function radial_influence(t::T, D::T, H::T, q::T)::Array{T,1} where {T<:AbstractFloat}
    k = 3.1;
    ro = 870;
    cp = 2300.;
    αg  = k/ro/cp;

    r = collect(1.:1:300);

    dT =  q/(2*pi*k) .* fls(t, αg, r, D, D, H, H)
end


"""
    temporal_influence(r::T, D::T, H::T, q::T)::Array{T,1} where {T<:AbstractFloat}

Calculate the ground temperature change due to the operation of the borehole considered at a given distance from the borehole as a function of time

# Arguments

- t: time at which the influence is calculated [s]
- D: borehole buried depth [m]
- H: borehole length [m]
- q: borehole yearly extraction load [W]
"""
function temporal_influence(r::T, D::T, H::T, q::T)::Array{T,1} where {T<:AbstractFloat}
    k = 3.1;
    ro = 870;
    cp = 2300.;
    αg  = k/ro/cp;

    t = collect(1.:1:100) .* 8760 * 3600;

    dT =  q/(2*pi*k) .* fls(t, αg, r, D, D, H, H)
end
