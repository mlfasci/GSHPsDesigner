"""
Returns the 2D coordinates of R-spaced points in a square grid of side length L
"""
function square(R::T,L::T) where {T<:AbstractFloat}
    x = y = 0:R:L
    P0   =  unique([Point2{Float64}(xi,yi) for xi in x, yi in y])
end
"""
Returns the 2D coordinates of R-spaced points in a square grid of N points per side
"""
function square(R::T,N::Int64) where {T<:AbstractFloat}
    P0   =  square(R,R*(N-1))
end
"""
Returns the 2D coordinates of R-spaced points in a rectangular grid of N x M points
"""
function rectangle(R::T,N::Int64, M::Int64) where {T<:AbstractFloat}
    L1 = R * (N-1);
    L2 = R * (M-1);
    x = 0:R:L1;
    y = 0:R:L2;
    P0   =  unique([Point2{Float64}(xi,yi) for xi in x, yi in y])
end

"""
Returns the 2D coordinates of d-spaced points in a square grid of side 2R included in a circular grid of radius R.
The function is built so that the circle is centered on a point of the grid.
"""
function circle(d::T,R::T) where {T<:AbstractFloat}
    isequal(mod(R,d),0) ? R2=R : R2=R+(d - mod(R,d))
    #isequal(mod(R,d),0) ? R2=R : R2=R+(mod(R,d))
    P0 = square(d,2*R2);
    c0 = hcat(collect(Array(p) for p in P0)...);
    c=size(c0,2)
    for i=1:c
        euclidean([R2,R2],c0[:,i])>R ? P0[i]=Point2{Float64}(-1.,-1.) : P0[i]=P0[i]
    end
    P0 = unique(P0)[2:end];
end

"""
Returns the 3D coordinates of R-spaced points in a square grid of side length L, the z coordinate is set for all the points to D
"""
function prism(R::T,L::T, D::T) where {T<:AbstractFloat}
    x = y = 0:R:L
    P0   =  unique([Point3{Float64}(xi,yi,D) for xi in x, yi in y])
end

"""
Returns the 3D coordinates of R-spaced points in a prism grid of side length L, the m  z-coordinates vary from D to D+H with step H/m.
"""
function prism(R::T,L::T, D::T, H::T, m::Int64) where {T<:AbstractFloat}
    x = y = 0:R:L

    h=H/m;
    z = D:h:D+h*(m-1);

    P0   =  unique([Point3{Float64}(xi,yi,zi) for zi in z, xi in x, yi in y])
end

"""
Returns the 3D coordinates of R-spaced points in a square grid of N points per side, the z coordinate is set for all the points to D
"""
function prism(R::T,N::Int64, D::T) where {T<:AbstractFloat}
    P0   =  prism(R,R*(N-1),D)
end

"""
Returns the 3D coordinates of R-spaced points in a square grid of N points per side, the m  z-coordinates vary from D to D+H with step H/m.
"""
function prism(R::T,N::Int64, D::T, H::T, m::Int64) where {T<:AbstractFloat}
    P0   =  prism(R,R*(N-1),D, H,m)
end

"""
Returns the 3D coordinates of R-spaced points in a rectangular grid of N x M points, the m  z-coordinates vary from D to D+H with step H/m.
"""
function prism(R::T,N::Int64, M::Int64, D::T, H::T, m::Int64) where {T<:AbstractFloat}
    L1 = R * (N-1);
    L2 = R * (M-1);
    x = 0:R:L1;
    y = 0:R:L2;

    h=H/m;
    z = D:h:D+h*(m-1);

    P0   =  unique([Point3{Float64}(xi,yi,zi) for zi in z, xi in x, yi in y])
end


"""
Returns the 3D coordinates of d-spaced points in a square grid of side 2R included in a circular area of radius R, the z coordinate is set for all the points to D
The function is built so that the circle is centered on a point of the grid.
"""
function cylinder(d::T,R::T, D::T) where {T<:AbstractFloat}
    isequal(mod(R,d),0) ? R2=R : R2=R+(d - mod(R,d))
    P0 = prism(d,2*R2,D);
    c0 = hcat(collect(Array(p) for p in P0)...);
    c=size(c0,2)
    for i=1:c
        euclidean([R2,R2,D],c0[:,i])>R ? P0[i]=Point3{Float64}(-1.,-1.,-1.) : P0[i]=P0[i]
    end
    P0 = unique(P0)[2:end];
end

"""
Returns the 3D coordinates of d-spaced points in a square grid of side 2R included in a circular area of radius R, the m z-coordinates vary from D to D+H with step H/m.
The function is built so that the circle is centered on a point of the grid.
"""
function cylinder(d::T,R::T, D::T, H::T, m) where {T<:AbstractFloat}
    isequal(mod(R,d),0) ? R2=R : R2=R+(d - mod(R,d))
    P0 = prism(d,2*R2,D, H, m);
    c0 = hcat(collect(Array(p) for p in P0)...);
    c=length(P0)
    for i=1:c
        euclidean([R2,R2],c0[1:2,i])>R ? P0[i]=Point3{Float64}(0.,0.,0.) : P0[i]=P0[i]
    end
    P0 = unique(P0)[2:end];
end
