# GSHPsDesigner

This is an open-source software for calculating the thermal interference between independent Ground Source Heat Pumps (GSHPs) in densely populated areas.

The software is under development and many more features will be added to make it a complete tool for the design of GSHPs taking into account technical and economical performance.
## Installation

**IMPORTANT!** If you don't have administration rights on your computer download the **portable** version, not the installer version!<br/>
**1.** [Download](https://julialang.org/downloads/) the latest release of Julia. Check your Operating System version to download the correct version of Julia. For example, we needed the 64-bit version for Windows.<br/>
<br/>

<div align="center">
  <img width="600" height="400" src="./assets/JuliaInst_2.jpg">
  <br/>

**Fig.1 Julia download page**<br/>
</div>
<br/>


**2.** Double click on the Julia executable you have just downloaded to start the installation.<br/>
<br/>
<kbd>
<div align="center">
<img src="./assets/JuliaInst_3.jpg" width="50%" height="50%" /><br/>
</kbd>
<br/>

**Fig.2 Julia executable**</em><br/>
</div>
<br/>

**3.** Install Julia. Use the options selected by default<br/>


**4.** Start Julia. Figure 4 shows hot it should look like.<br/>
<br/>
<kbd>
<div align="center">
<img src="./assets/JuliaInst_5.png" width="600" height="400"/><br/>
</kbd>
<br/>
<em>

**Fig.3 Julia window**</em><br/>

</div>
<br/>

**5.** Type:

                ] 

This will activate the julia package manager

**6.** Write:

              add https://gitlab.com/mlfasci/GSHPsDesigner

This will install GSHPsDesigner. It may take some minutes.

**7.** Press backspace to exit the package manager

**8.** Type:

                using GSHPsDesigner


**Congratulations, you can now use GSHPsDesigner!** 

## Use (once installed)

**1.** Start julia by double-clicking its icon (like any regular software)

**2.** Write:

                using GSHPsDesigner

**Congratulations, you can now use GSHPsDesigner!** 




## Developers

The software is being developed at the Department of Energy Technology - KTH - Royal Institute of Technology, Stockholm, by Maria Letizia Fascì (mlfasci@kth.se).

## Acknowledgments 
This project is supported by the Swedish Energy Agency (Energimyndigheten) under grant P43647-3.

It is developed in partnership with Bengt Dahlgren Geoenergi AB, Borrföretagen i Sverige ek.för., Neoenergy Sweden AB, Nibe AB, Nowab AB, Stockholms kommun, Svenskt geoenergicentrum AB, Thermia AB, Täby kommun.




